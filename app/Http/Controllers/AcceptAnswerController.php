<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use Illuminate\Support\Facades\Gate;

class AcceptAnswerController extends Controller
{
    public function __invoke(Answer $answer){

        if(Gate::allows('accept-answer',$answer)){
            $answer->question->acceptBestAnswer($answer);
            if(request()->expectsJson()){
                return response()->json([
                    'message' => "You have accepted this answer as best answer"
                ]);
            }
            return back();    
        };
        abort(403,"Access denied");
    }
}
