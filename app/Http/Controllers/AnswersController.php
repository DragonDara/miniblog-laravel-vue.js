<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;
use App\Question;
use Illuminate\Support\Facades\Gate;

class AnswersController extends Controller
{
    
    public function __construct(){
        $this->middleware('auth')->except('index');
    }
    public function index(Question $question){
        return $question->answers()->with('user')->simplePaginate(3);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Question $question,Request $request)
    {
        // $request->validate([
        //     'body'=>'required'
        // ]);

       $answer =  $question->answers()->create(['body'=>$request->body,'user_id'=>\Auth::id()]);

        if($request->expectsJson()){
            return response()->json([
                'message'=>'Your answer has been submitted succesfully',
                'answer' => $answer->load('user')
            ]);
        }
        //return back()->with('success','Your answer has been submitted succesfully');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question,Answer $answer)
    {
        if(Gate::allows('update-answer',$answer)){
            return view("answers.edit",compact('question','answer'));
        };
        abort(403,"Access denied");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question,Answer $answer)
    {
        if(Gate::allows('update-answer',$answer)){
            $answer->update($request->validate([
                'body'=>'required',
            ]));
            
            if($request->expectsJson()){
                return response()->json([
                    'message' =>'Your question has been updated',
                    'body_html'=>$answer->body_html
                ]);
            }
            
            //return redirect()->route('questions.show',$question->slug)->with('success','Your question has been updated');
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question,Answer $answer,Request $request)
    {
        if(Gate::allows('delete-answer',$answer)){
            $answer->delete();
            if($request->expectsJson()){
                return response()->json([
                    'message' =>'Your answer has been removed',
                ]);
            }
            //return redirect()->route('questions.show',$question->slug)->with('success',"Your message has been deleted");
            return back()->with('success','Your answer has been removed');
        };

        abort(403,"Access denied");
    }
}
